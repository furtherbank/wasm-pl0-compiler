
import { Parser } from '../compiler/Parser'

async function testCode (code: string, input: number[] = []) {
    const parser = new Parser()
    await parser.init()
    parser.analyze(code)
    expect(parser.tokens).toBeTruthy()
    const wat = parser.parse()
    
    expect(wat).toBeTruthy()
    const result = parser.compile(input)
    console.log(wat?.join('\n'))
    return result
}
test('mul-ok', async () => {
    const code = `
var z;
begin 
    z := z * 8
end.
`
    const r = await testCode(code,[12,7])
    expect(r[0]).toBe(15)
    expect(r.length).toBe(1)
    // fs.writeFileSync('F:/资料库/大三学期/编译原理/cpu-editor-master/files/simple.wat', wat?.join('\n'))
    // const result = parser.compile([35,94])
    // expect(result[0]).toBe(23)
    // expect(result[1]).toBe(12)
})
test('simple-ok', async () => {
    const code = `
const z = 4;
var a, b;
begin
    read(a, b);
    b := a + b - z;
    write(b)
end.
`
    const r = await testCode(code,[12,7])
    expect(r[0]).toBe(15)
    expect(r.length).toBe(1)
    // fs.writeFileSync('F:/资料库/大三学期/编译原理/cpu-editor-master/files/simple.wat', wat?.join('\n'))
    // const result = parser.compile([35,94])
    // expect(result[0]).toBe(23)
    // expect(result[1]).toBe(12)
})

test('closure-1layer', async () => {
    const code = `
const z = 4;
var a, b;
procedure add;
    var a;
    begin
        a := b + 1;
        write(a)
    end;
begin
    read(a, b);
    call add;
    write(a)
end.`
    const r = await testCode(code,[12,7])
    expect(r[0]).toBe(8)
    expect(r[1]).toBe(12)
    // fs.writeFileSync('F:/资料库/大三学期/编译原理/cpu-editor-master/files/simple.wat', wat?.join('\n'))
    // const result = parser.compile([35,94])
    // expect(result[0]).toBe(23)
    // expect(result[1]).toBe(12)
})

test('parser-jttl-ok', async () => {
    const code = `
var head, foot, cock, rabbit, n;
begin
    read(head, foot);
    cock := 1;
    while cock <= head do
    begin
        rabbit := head - cock;
        if cock * 2 + rabbit * 4 = foot then
        begin 
            write(cock, rabbit);
            n := n + 1
        end;
        cock := cock + 1
    end;
    if n = 0 then write(0, 0)
end.    
`
    const result = await testCode(code)
    expect(result[0]).toBe(0)
    expect(result[1]).toBe(0)
})


test('parser-proc-ok', async () => {
    const code = `
const a = 45,b = 27;
var x, y, g, m;
procedure swap;
    var temp;
    begin
        temp := x;
        x := y;
        y := temp
    end;
procedure mod;
    x := x - x / y * y;
begin
    x := a;
    y := b;
    call mod;
    while x <> 0 do
    begin
        call swap;
        call mod
    end;
    g := y;
    m := a * b / g;
    write(g, m)
end.  
`
    const result = await testCode(code,[35,94])
    // expect(result[0]).toBe(23)
    // expect(result[1]).toBe(12)
})


test('parser-fact-ok', async () => {
    const code = `
var n, fact;
begin
    read(n);
    fact := 1;
    repeat
        fact := fact * n;
        n := n - 1;
    until n <= 0;
    write(fact)
end.
`
    const result = await testCode(code,[5])
    expect(result[0]).toBe(120)
    // expect(result[1]).toBe(12)
})

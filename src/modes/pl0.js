const CodeMirror = require("codemirror/lib/codemirror")


CodeMirror.defineMode("pl0", function () {
    function words(str) {
        var obj = {}, words = str.split(" ");
        for (var i = 0; i < words.length; ++i) obj[words[i]] = true;
        return obj;
    }
    // 定义 keywords 的map(keyword=>true)
    var keywords = words(
        "begin call const do " +
        "else end if implementation odd" +
        "procedure " +
        "read repeat then " +
        "until var while write"
    );

    // 这个正则用来定义是否是运算符
    var isOperatorChar = /[+\-*&%=<>!?|\/\:]/;

    // Interface

    return {
        startState: function () {
            return {};
        },

        token: function (stream, state) {
            // 空格没有样式
            if (stream.eatSpace()) return null;
            // 按照下个字符样式来做
            var ch = stream.next();
            // 这些符号没有样式
            if (/[\[\]\(\),;\.]/.test(ch)) {
                return null;
            }
            if (/\d/.test(ch)) {
                // 数字
                stream.eatWhile(/[\w\.]/);
                return "number";
            }
    
            // 运算符
            if (isOperatorChar.test(ch)) {
                stream.eatWhile(isOperatorChar);
                return "operator";
            }
            // 一直吃单个字符
            stream.eatWhile(/[\w\$_]/);
            // 当前字符流判断是否是保留字
            var cur = stream.current();
            if (keywords.propertyIsEnumerable(cur)) return "keyword";   // 保留字
            // 其它为变量样式
            return "variable";
        },

    };
});


declare module 'libwabt.js' {
    export class Wabt {
        parseWat(name: string, wat: string, features: any): WasmModule
    }
    export class WasmModule {
        resolveNames(): void
        validate(features: any): void
        toBinary(param: any): BinaryOutput
    }
    export interface BinaryOutput {
        buffer: Uint8Array
        log: string
    }
    export default function WabtModule(): Promise<Wabt>
}
import { TokenType, keyword, operator } from "./Constrants"


export class Token {
    type: TokenType = 0
    value: any = ''
    
    constructor(type: TokenType, value: string) {
        // 细分字符类型
        switch (type) {
            case TokenType.IDF:     // IDF细分保留字
                const keywordIndex = keyword.indexOf(value)
                if (keywordIndex !== -1) {
                    this.type = keywordIndex + TokenType.BGN
                }
                break;
            case TokenType.NUM:     // NUM存储数字值
                this.type = type
                break;
            case TokenType.EQL:     // EQL进行符号细分
                const operatorIndex = operator.indexOf(value)
                if (operatorIndex !== -1) {
                    this.type = operatorIndex + TokenType.EQL
                }
                break;
            default:
                this.type = type
                break;
        }
        this.value = value  
    }
}
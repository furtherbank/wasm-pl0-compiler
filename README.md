# 项目结构与代码
## 项目架构
该项目使用TypeScript作为该项目的实现编程语言。  
在该项目中，使用了开源代码编辑器codemirror作为项目的代码输入框，并定义了PL/0代码的编辑模式，编辑时具有代码高亮、代码块折叠以及单词颜色分类的功能。  
中间代码为WebAssembly的文本格式(wat)，该格式类似于汇编，和二进制代码一一对应。最终执行阶段会将其直接转换为wasm二进制指令代码并通过WebAssembly虚拟机直接执行。
## 安装与执行
可以直接打开 [http://furtherbank.gitee.io/wasm-pl0-compiler](http://furtherbank.gitee.io/wasm-pl0-compiler) 进入编译器界面，在页面中编写pl0代码并执行。
如果需要编辑源代码，请按以下步骤操作：
1. 首先确保计算机安装了nodejs，然后输入命令`npm install`安装所有项目依赖
2. 运行`npm start`，等待本地服务器启动，这时在浏览器输入`localhost:3000`即可进入编译器界面。  
启动本地服务器时间稍长，如果浏览器因超时原因未能加载，稍后刷新页面即可。
## 源代码以及架构
编译器相关都放在`src/compiler`文件夹下。  
词法分析，语法分析，代码执行的所有逻辑在`Parser.ts`中。  
编译过程中需要的数据结构和常量定义在`src/common`文件夹下。  
`src/modes`文件夹下包含codemirror编辑器需要的pl0代码模式文件。  
